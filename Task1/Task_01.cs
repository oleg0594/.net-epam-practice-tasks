using System;

namespace Task_01
{	 
    public sealed class Vector: IComparable<Vector>
    {
        #region Constructors
        public Vector(double x, double y, double z)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
        }
        #endregion
        
        #region Properties
        public double X { get; private set; }
        public double Y { get; private set; }
        public double Z { get; private set; }
        
        //Calculates length of vector
        public double Length
        {
            get
            {
                return Math.Sqrt(this.X * this.X + this.Y * this.Y + this.Z * this.Z);
            }
        }
        #endregion
        
        #region Methods
        // Addition of two vectors.
        public static Vector Add(Vector vector1, Vector vector2)
        {
            if (vector1 == null || vector2 == null)
                throw new ArgumentNullException();
            
            return new Vector(vector1.X + vector2.X, vector1.Y + vector2.Y, vector1.Z + vector2.Z);
        }
        
        public Vector Add(Vector vector)
        {            
            if (vector == null)
                throw new ArgumentNullException();
            
            return Vector.Add(this, vector);
        }
        
        // Substraction of two vectors.
        public static Vector Substract(Vector vector1, Vector vector2)
        {
            if (vector1 == null || vector2 == null)
                throw new ArgumentNullException();
            
            return new Vector(vector1.X - vector2.X, vector1.Y - vector2.Y, vector1.Z - vector2.Z);
        }
        
        public Vector Substract(Vector vector)
        {
            if (vector == null)
                throw new ArgumentNullException();
            
            return Vector.Substract(this, vector);
        }
        
        // Scalar product of two vectors.
        public static double ScalarProduct(Vector vector1, Vector vector2)
        {
            if (vector1 == null || vector2 == null)
                throw new ArgumentNullException();
            
            return vector1.X * vector2.X + vector1.Y * vector2.Y + vector1.Z * vector2.Z;
        }
        
        public double ScalarProduct(Vector vector)
        {
            if (vector == null)
                throw new ArgumentNullException();
            
            return Vector.ScalarProduct(this, vector);
        }
        
        // Vector product of two vectors.
        public static Vector VectorProduct(Vector vector1, Vector vector2)
        {
            if (vector1 == null || vector2 == null)
                throw new ArgumentNullException();
            
            return new Vector(vector1.Y * vector2.Z - vector1.Z * vector2.Y, 
                              vector1.Z * vector2.X - vector1.X * vector2.Z,
                              vector1.X * vector2.Y - vector1.Y * vector2.X);
        }
        
        public Vector VectorProduct(Vector vector)
        {
            if (vector == null)
                throw new ArgumentNullException();
            
            return Vector.VectorProduct(this, vector);
        }
        
        // Triple product of vectors.
        public static double TripleProduct(Vector vector1, Vector vector2, Vector vector3)
        {
            if (vector1 == null || vector2 == null || vector3 == null)
                throw new ArgumentNullException();
            
            return vector1.X * vector2.Y * vector3.Z +
                   vector1.Y * vector2.Z * vector3.X +
                   vector1.Z * vector2.X * vector3.Y -
                   vector1.Z * vector2.Y * vector3.X -
                   vector1.Y * vector2.X * vector3.Z -
                   vector1.X * vector2.Z * vector3.Y;
        }
        
        // Calculates the angle between two vectors.
        public static double AngleBetween(Vector vector1, Vector vector2)
        {
            if (vector1 == null || vector2 == null)
                throw new ArgumentNullException();
            
            // May be division by zero !!!.
            return (Vector.ScalarProduct(vector1, vector2)) / (vector1.Length * vector2.Length);
        }
        
        public double AngleBetween(Vector vector)
        {
            if (vector == null)
                throw new ArgumentNullException();

            return Vector.AngleBetween(this, vector);
        }
        
        public override bool Equals(object other)
        {
            bool result = false;

            if (other is Vector)
            {
                result = (this.Length == ((Vector)(other)).Length);
            }

            return result;
        }
        
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        
        public int CompareTo(Vector other)
        {
            if (other == null) return 1;
            
            return this.Length.CompareTo(other.Length);
        }

        public override string ToString()
        {
            return String.Format("[{0}; {1}; {2}]", this.X, this.Y, this.Z);
        }
        #endregion
        
        #region Operators
        // Vector addition.
        public static Vector operator +(Vector vector1, Vector vector2)
        {
            return Vector.Add(vector1, vector2);
        }
       
        // Subtract vectors.
        public static Vector operator -(Vector vector1, Vector vector2)
        {
            return Vector.Substract(vector1, vector2);
        }
       
        // Vector product of two vectors.
        public static Vector operator *(Vector vector1, Vector vector2)
        {
            return Vector.VectorProduct(vector1, vector2);
        }
        
        public static bool operator <(Vector lVector, Vector rVector)
        {
            return (lVector.Length < rVector.Length);
        }
        
        public static bool operator <=(Vector lVector, Vector rVector)
        {
            return (lVector.Length < rVector.Length || lVector == rVector);
        }
       
        public static bool operator >(Vector lVector, Vector rVector)
        {
            return (lVector.Length > rVector.Length);
        }
        
        public static bool operator >=(Vector lVector, Vector rVector)
        {
            return (lVector.Length > rVector.Length || lVector == rVector);
        }
        
        public static bool operator ==(Vector lVector, Vector rVector)
        {
            if ((object)lVector == null || (object)rVector == null)
                return false;
            
            return (lVector.Length == rVector.Length);
        }
        
        public static bool operator !=(Vector lVector, Vector rVector)
        {
            if ((object)lVector == null || (object)rVector == null)
                return false;
            
            return (lVector.Length != rVector.Length);
        }
        #endregion
    }
	
    public class Program
    {
        public static void Main()
        {
            Vector vector1 = new Vector(10, 12, 15);
            Vector vector2 = new Vector(7, 7, 7);
            Vector vector3 = new Vector(1, 0, 1);
            Vector res;

            Console.WriteLine("1st vector: {0}", vector1);
            Console.WriteLine("2nd vector: {0}", vector2);
            Console.WriteLine("3rd vector: {0}", vector3);
            Console.WriteLine();

            res = vector1 + vector2;
            // res = vector1.Add(vector2).Add(vector3);
            Console.WriteLine("Addition of two vectors is {0}", res);

            res = vector1 - vector2;
            Console.WriteLine("Substraction of two vectors is {0}", res);

            res = vector1 * vector2;
            // The same is
            // res = Vector.VectorProduct(vector1, vector2);
            Console.WriteLine("Product of two vectors is {0}", res);
            
            double dValue = Vector.ScalarProduct(vector1, vector2);
            Console.WriteLine("Scalar product of 1st and 2nd vectors is {0}", dValue);

            dValue = Vector.TripleProduct(vector1, vector2, vector3);
            Console.WriteLine("Triple product 1st, 2nd, 3rd vectors - {0}", res);

            dValue = Vector.AngleBetween(vector1, vector2);
            Console.WriteLine("Angle between 1st & 2nd vectors is {0}", dValue);

            Console.WriteLine("1st vector > 2nd vector? [{0}]", vector1 > vector2);
            Console.WriteLine("1st vector < 3rd vector? [{0}]", vector1 < vector3);
        }
    }
}